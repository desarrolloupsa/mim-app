angular.module('mimApp', ['ngRoute', 'ngResource', 'ui.bootstrap'])
  .run(function ($rootScope) {
    $rootScope.appSettings = {
      pageCountLimit: 15
    };
  });
