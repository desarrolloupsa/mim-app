angular.module('mimApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/main/templates/main-menu.html',
        controller: 'MainCtrl'
      });
  });
