angular.module('mimApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/empleados', {
        templateUrl: 'app/employee/templates/employee-list.html',
        controller: 'EmployeeListCtrl'
      })
      .when('/empleados/nuevo', {
        templateUrl: 'app/employee/templates/employee-editor.html',
        controller: 'EmployeeEditorCtrl'
      })
      .when('/empleados/:employeeId', {
        templateUrl: 'app/employee/templates/employee-detail.html',
        controller: 'EmployeeDetailCtrl'
      })
      .when('/empleados/:employeeId/modificar', {
        templateUrl: 'app/employee/templates/employee-editor.html',
        controller: 'EmployeeEditorCtrl'
      });
  });
