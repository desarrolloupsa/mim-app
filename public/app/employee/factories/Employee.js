angular.module('mimApp')
  .factory('Employee', function (Resource) {
    var params = {
    };
    
    var actions = {
    };
    
    return Resource('/api/employees/:id', params, actions, [transform]);
    
    function transform(employee) {
      employee.birthday = moment(employee.birthday).toDate();
      return employee;
    }
  });