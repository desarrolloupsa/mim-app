angular.module('mimApp')
  .directive('employeeEditor', function ($http) {
    return {
      scope: {
        employee: '=',
        onSave: '&',
        onCancel: '&'
      },
      restrict: 'E',
      templateUrl: 'app/employee/directives/employee-editor.html',
      link: link
    };
    
    function link($scope, $elem, $attr) {
      $scope.submitted = false;
      $scope.sending = false;
      $scope.success = false;
      
      $scope.handleSubmit = function () {
        $scope.submitted = true;
        $scope.sending = true;
        
        $scope.employee.$save()
          .then(function () {
            $scope.success = true;
            
            $scope.onSave();
          })
          .catch(function () {
            alert('Hubo un error. No se guardaron los cambios.');
          })
          .finally(function () {
            $scope.sending = false;
          });
      };
      
      $scope.handleCancelClick = function () {
        $scope.onCancel();
      };
    }
  });
