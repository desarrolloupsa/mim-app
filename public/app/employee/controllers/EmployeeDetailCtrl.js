angular.module('mimApp')
  .controller('EmployeeDetailCtrl', function ($scope, $routeParams, $http, $location, $timeout, Employee, Product, Receipt) {
    var employeeId = $routeParams.employeeId;
    
    $scope.deleted = false;
    
    $scope.employee = Employee.get({ id: employeeId });
    
    $scope.employee.$promise.catch(function () {
      $scope.redirectToList();
    });
    
    $scope.receiptLinesByDay = [];
    
    $scope.dateRange = {
      from: moment().startOf('isoWeek').toDate(),
      to: moment().startOf('isoWeek').add(7, 'days').toDate()
    };
    
    $scope.redirectToList = function () {
      $location.path('/empleados');
    };
    
    $scope.fetchReceiptLines = function () {
      var timezone = moment().utcOffset() / 60;
      var fromDate = moment($scope.dateRange.from);
      var toDate = moment($scope.dateRange.to);
      
      $http.get('/api/employees/' + employeeId + '/receipt-lines/by-day', {
        params: {
          timezone: timezone,
          from: fromDate.toISOString(),
          to: toDate.toISOString()
        }
      })
      .success(function (data) {
        $scope.receiptLinesByDay = data;
        
        $scope.receiptLinesByDay.forEach(function (lineGroup) {
          lineGroup.lines.forEach(function (line) {
            line.receipt = Receipt.get({ id: line.receiptId });
            line.product = Product.get({ id: line.productId });
          });
        });
      });
    };
    
    $scope.getLineCommissionValue = function (line) {
      return line.price * line.commissionRate - line.commissionDiscount;
    };
    
    $scope.getPriceTotal = function (lineGroup) {
      var total = 0;
      lineGroup.lines.forEach(function (line) {
        total += line.price;
      });
      return total;
    };
    
    $scope.getSalesCommissionTotal = function (lineGroup) {
      var total = 0;
      lineGroup.lines.forEach(function (line) {
        total += $scope.getLineCommissionValue(line);
      });
      return total;
    };
    
    $scope.inputCommissionRate = function (line) {
      var value = prompt('% Comision:', line.commissionRate * 100);
      
      if (!value) return;
      
      line.commissionRate = Number(value) / 100;
      
      $http.put('/api/receipt-lines/' + line._id, { commissionRate: line.commissionRate })
        .success(function () {
          
        })
        .error(function () {
          alert('No se pudieron guardar los cambios.')
        });
    };
    
    $scope.inputCommissionDiscount = function (line) {
      var value = prompt('Descuento:', line.commissionDiscount);
      
      if (!value) return;
      
      line.commissionDiscount = Number(value);
      
      $http.put('/api/receipt-lines/' + line._id, { commissionDiscount: line.commissionDiscount })
        .success(function () {
          
        })
        .error(function () {
          alert('No se pudieron guardar los cambios.')
        });
    };
    
    $scope.inputRemunerated = function (line) {
      $http.put('/api/receipt-lines/' + line._id, { remunerated: line.remunerated })
        .success(function () {
          
        })
        .error(function () {
          alert('No se pudieron guardar los cambios.')
        });
    };
    
    $scope.handleDeleteClick = function () {
      if (!confirm('Esta operacion no se puede deshacer. Desea continuar?'))
        return;
      
      $http.delete('/api/employees/' + $scope.employee._id)
        .success(function () {
          $scope.deleted = true;
          $timeout($scope.redirectToList, 1000);
        });
    };
    
    // this will invoke fetch on start
    $scope.$watch('dateRange', $scope.fetchReceiptLines, true);
  });
