angular.module('mimApp')
  .controller('EmployeeEditorCtrl', function ($scope, $routeParams, $location, $timeout, Employee) {
    var employeeId = $routeParams.employeeId;
    
    $scope.editing = !!employeeId;
    $scope.creating = !employeeId;
    
    $scope.employee = new Employee();
    
    if (employeeId) {
      $scope.employee = Employee.get({ id: employeeId });
      
      $scope.employee.$promise.catch(function () {
        $scope.redirectToList();
      });
    }
    
    $scope.redirectToList = function () {
      $location.path('/empleados');
    };
    
    $scope.redirectToDetail = function () {
      $location.path('/empleados/' + $scope.employee._id);
    };
    
    $scope.goBack = function () {
      if ($scope.editing) {
        $scope.redirectToDetail();
      } else {
        $scope.redirectToList();
      }
    };
    
    $scope.onSave = function () {
      $timeout($scope.goBack, 1000);
    };
    
    $scope.onCancel = function () {
      $scope.goBack();
    };
  });
