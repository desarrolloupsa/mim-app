angular.module('mimApp')
  .controller('EmployeeListCtrl', function ($scope, $http, $location) {
    var self = $scope;
    
    self.tableColumns = [
      { key: 'name',        name: 'Nombre' },
      { key: 'phoneNumber', name: 'Tel.' }
    ];
    
    self.handleEmployeeClick = function (employee) {
      $location.path('/empleados/' + employee._id);
    };
  });
