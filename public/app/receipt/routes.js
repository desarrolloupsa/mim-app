angular.module('mimApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/comandas', {
        templateUrl: 'app/receipt/templates/receipt-list.html',
        controller: 'ReceiptListCtrl'
      })
      .when('/comandas/nuevo', {
        templateUrl: 'app/receipt/templates/receipt-editor.html',
        controller: 'ReceiptEditorCtrl'
      })
      .when('/comandas/:id', {
        templateUrl: 'app/receipt/templates/receipt-detail.html',
        controller: 'ReceiptDetailCtrl'
      });
  });
