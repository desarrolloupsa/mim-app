angular.module('mimApp')
  .factory('Receipt', function (Resource) {
    var params = {
    };
    
    var actions = {
    };
    
    return Resource('/api/receipts/:id', params, actions, []);
  });