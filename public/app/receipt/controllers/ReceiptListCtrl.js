angular.module('mimApp')
  .controller('ReceiptListCtrl', function ($scope, $http, $location, $q, $filter) {
    var self = $scope;
    
    self.tableColumns = [
      { key: 'createdAt',     name: 'Fecha y hora' },
      { key: 'code',          name: 'No.' },
      { key: 'clientName',    name: 'Cliente' },
      { key: 'linesTotal',    name: 'Total (Bs.)', noOrder: true }
    ];
    
    self.preprocessReceipt = function (receipt) {
      receipt.createdAt = $filter('momentFmt')(receipt.createdAt, 'YYYY-MM-DD HH:mm');
      
      $http.get('/api/clients/' + receipt.clientId)
        .success(function (client) {
          receipt.clientName = client.name;
        });
      
      $http.get('/api/receipts/' + receipt._id + '/lines/total')
        .success(function (total) {
          receipt.linesTotal = $filter('currency')(total.value, '');
        });
    };
    
    self.handleReceiptClick = function (receipt) {
      $location.path('/comandas/' + receipt._id);
    };
  });
