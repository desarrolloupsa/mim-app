angular.module('mimApp')
  .controller('ClientEditorModalCtrl', function ($scope, $modalInstance, Client) {
    $scope.client = new Client();
    
    $scope.onSave = function () {
      $modalInstance.close($scope.client);
    };
    
    $scope.onCancel = function () {
      $modalInstance.dismiss('cancel');
    };
  })
  .controller('ReceiptEditorCtrl', function ($scope, $modal, $http, $location, $timeout, $q) {
    var self = $scope;
    
    self.submitted = false;
    self.sending = false;
    self.success = false;
    
    self.receipt = {
      createdAt: new Date(moment().utc().format('YYYY-MM-DDTHH:mmZ'))
    };
    self.lines = [];
    
    self.redirectToList = function () {
      $location.path('/comandas');
    };
    
    self.redirectToDetail = function () {
      $location.path('/comandas/' + self.receipt._id);
    };
    
    self.getLinesTotal = function () {
      var total = 0;
      self.lines.forEach(function (line) {
        total += line.price || 0;
      });
      return total;
    };
    
    // --- events
    
    self.$on('$locationChangeStart', function (ev) {
      if (self.form.$dirty && !self.success) {
        if (!confirm('Perdera todos sus cambios. Esta seguro?')) {
          ev.preventDefault();
        }
      }
    });
    
    self.handleSubmit = function () {
      self.submitted = true;
      self.sending = true;
      
      $http.post('/api/receipts', self.receipt)
        .success(function (receipt) {
          // TODO: omg dat transaction
          var reqs = self.lines.map(function (line) {
            line.receiptId = receipt._id;
            line.createdAt = receipt.createdAt;
            return $http.post('/api/receipt-lines', line);
          });
          
          $q.all(reqs)
            .then(function () {
              self.success = true;
              $timeout(self.redirectToList, 1000);
            }, function () {
              alert('Hubo un error. No se guardaron los cambios.');
            });
        })
        .error(function () {
          alert('Hubo un error. No se guardaron los cambios.');
        })
        .finally(function () {
          self.sending = false;
        });
    };
    
    self.handleCancelClick = function () {
      self.redirectToList();
    };
    
    self.handleLineAddClick = function () {
      self.lines.push({ });
      
      self.form.$setValidity('noLines', true);
    };
    
    self.handleLineDeleteClick = function (i, line) {
      self.lines.splice(i, 1);
      
      if (self.lines.length === 0) {
        self.form.$setValidity('noLines', false);
      }
    };
    
    self.showClientEditor = function () {
      var modal = $modal.open({
        templateUrl: 'client-editor-modal.html',
        controller: 'ClientEditorModalCtrl'
      })
      
      modal.result.then(function (client) {
        self.receipt.clientId = client._id;
      });
    };
    
    // -- init
    
    // when the form gets bound to the scope
    self.$watch('form', function () {
      self.handleLineAddClick();
    });
  });
