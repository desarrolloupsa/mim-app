angular.module('mimApp')
  .controller('ReceiptDetailCtrl', function ($scope, $routeParams, $http, $location, $timeout, $q) {
    var receiptId = $routeParams.id;
    
    $scope.loading = false;
    $scope.deleted = false;
    $scope.receipt = {};
    $scope.receiptLines = [];
    
    $scope.redirectToList = function () {
      $location.path('/comandas');
    };
    
    $scope.fetchReceipt = function () {
      $scope.loading = true;
      
      $http.get('/api/receipts/' + receiptId)
        .success(function (data) {
          data.createdAt = moment(data.createdAt).toDate();
          
          $scope.receipt = data;
          $scope.loading = false;
          
          $scope.fetchClient();
          $scope.fetchReceiptLines();
        })
        .error(function () {
          $scope.redirectToList();
        });
    };
    
    $scope.fetchClient = function () {
      $http.get('/api/clients/' + $scope.receipt.clientId)
        .success(function (data) {
          $scope.receipt.client = data;
        });
    };
    
    $scope.fetchReceiptLines = function () {
      $http.get('/api/receipt-lines', {
        params: {
          query: { receiptId: receiptId }
        }
      }).success(function (data) {
        $scope.receiptLines = data;
        
        var reqs1 = $scope.receiptLines.map(function (line) {
          line.product = {};
          return $http.get('/api/products/' + line.productId);
        });
        
        $q.all(reqs1).then(function (responses) {
          responses.forEach(function (response, i) {
            $scope.receiptLines[i].product = response.data;
          });
        });
        
        var reqs2 = $scope.receiptLines.map(function (line) {
          line.employee = {};
          return $http.get('/api/employees/' + line.employeeId);
        });
        
        $q.all(reqs2).then(function (responses) {
          responses.forEach(function (response, i) {
            $scope.receiptLines[i].employee = response.data;
          });
        });
      });
    };
    
    $scope.getLinesTotal = function () {
      var total = 0;
      $scope.receiptLines.forEach(function (line) {
        total += line.price || 0;
      });
      return total;
    };
    
    $scope.handleDeleteClick = function () {
      if (!confirm('Esta operacion no se puede deshacer. Desea continuar?'))
        return;
      
      $http.delete('/api/receipts/' + $scope.receipt._id)
        .success(function () {
          $scope.deleted = true;
          $timeout($scope.redirectToList, 1000);
        });
    };
    
    $scope.fetchReceipt();
  });
