angular.module('mimApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/clientes', {
        templateUrl: 'app/client/templates/client-list.html',
        controller: 'ClientListCtrl'
      })
      .when('/clientes/calendario', {
        templateUrl: 'app/client/templates/client-calendar.html',
        controller: 'ClientCalendarCtrl'
      })
      .when('/clientes/nuevo', {
        templateUrl: 'app/client/templates/client-editor.html',
        controller: 'ClientEditorCtrl'
      })
      .when('/clientes/:clientId', {
        templateUrl: 'app/client/templates/client-detail.html',
        controller: 'ClientDetailCtrl'
      })
      .when('/clientes/:clientId/modificar', {
        templateUrl: 'app/client/templates/client-editor.html',
        controller: 'ClientEditorCtrl'
      });
  });
