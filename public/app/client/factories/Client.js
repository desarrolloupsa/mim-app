angular.module('mimApp')
  .factory('Client', function (Resource) {
    var params = {
    };
    
    var actions = {
    };
    
    return Resource('/api/clients/:id', params, actions, [transform]);
    
    function transform(client) {
      client.birthday = moment(client.birthday).toDate();
      return client;
    }
  });