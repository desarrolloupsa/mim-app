angular.module('mimApp')
  .controller('ClientDetailCtrl', function ($scope, $routeParams, $location, $timeout, Client) {
    var clientId = $routeParams.clientId;
    
    $scope.deleted = false;
    
    $scope.client = Client.get({ id: clientId });
    
    $scope.client.$promise.catch(function () {
      $scope.redirectToList();
    });
    
    $scope.redirectToList = function () {
      $location.path('/clientes');
    };
    
    $scope.handleDeleteClick = function () {
      if (!confirm('Esta operacion no se puede deshacer. Desea continuar?'))
        return;
      
      $scope.client.$delete(function () {
        $scope.deleted = true;
        $timeout($scope.redirectToList, 1000);
      });
    };
  });
