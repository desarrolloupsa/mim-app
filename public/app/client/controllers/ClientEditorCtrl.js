angular.module('mimApp')
  .controller('ClientEditorCtrl', function ($scope, $routeParams, $location, $timeout, Client) {
    var clientId = $routeParams.clientId;
    
    $scope.editing = !!clientId;
    $scope.creating = !clientId;
    
    $scope.client = new Client();
    
    if (clientId) {
      $scope.client = Client.get({ id: clientId });
      
      $scope.client.$promise.catch(function () {
        $scope.redirectToList();
      });
    }
    
    $scope.redirectToList = function () {
      $location.path('/clientes');
    };
    
    $scope.redirectToDetail = function () {
      $location.path('/clientes/' + $scope.client._id);
    };
    
    $scope.goBack = function () {
      if ($scope.editing) {
        $scope.redirectToDetail();
      } else {
        $scope.redirectToList();
      }
    };
    
    $scope.onSave = function () {
      $timeout($scope.goBack, 1000);
    };
    
    $scope.onCancel = function () {
      $scope.goBack();
    };
  });
