angular.module('mimApp')
  .controller('ClientListCtrl', function ($scope, $http, $location) {
    $scope.tableColumns = [
      { key: 'name',        name: 'Nombre' },
      { key: 'phoneNumber', name: 'Tel.' }
    ];
    
    $scope.handleClientClick = function (client) {
      $location.path('/clientes/' + client._id);
    };
  });
