angular.module('mimApp')
  .directive('clientEditor', function ($http) {
    return {
      scope: {
        client: '=',
        onSave: '&',
        onCancel: '&'
      },
      restrict: 'E',
      templateUrl: 'app/client/directives/client-editor.html',
      link: link
    };
    
    function link($scope, $elem, $attr) {
      $scope.submitted = false;
      $scope.sending = false;
      $scope.success = false;
      
      $scope.handleSubmit = function () {
        $scope.submitted = true;
        $scope.sending = true;
        
        $scope.client.$save()
          .then(function () {
            $scope.success = true;
            
            $scope.onSave();
          })
          .catch(function () {
            alert('Hubo un error. No se guardaron los cambios.');
          })
          .finally(function () {
            $scope.sending = false;
          });
      };
      
      $scope.handleCancelClick = function () {
        $scope.onCancel();
      };
    }
  });
