angular.module('mimApp')
  .directive('clientNavbar', function ($http) {
    return {
      scope: {
        active: '='
      },
      restrict: 'E',
      templateUrl: 'app/client/directives/client-navbar.html',
      link: link
    };
    
    function link($scope) {
      $scope.activities = [
        {
          name: 'listado',
          url: '/clientes',
          title: 'Listado'
        },
        {
          name: 'calendario',
          url: '/clientes/calendario',
          title: 'Calendario'
        }
      ];
    }
  });