angular.module('mimApp')
  .factory('Product', function (Resource) {
    var params = {
    };
    
    var actions = {
    };
    
    return Resource('/api/products/:id', params, actions, []);
  });