angular.module('mimApp')
  .controller('ProductEditorCtrl', function ($scope, $routeParams, $location, $timeout, Product) {
    var productId = $routeParams.productId;
    
    $scope.editing = !!productId;
    $scope.creating = !productId;
    
    $scope.product = new Product();
    
    if (productId) {
      $scope.product = Product.get({ id: productId });
      
      $scope.product.$promise.catch(function () {
        $scope.redirectToList();
      });
    }
    
    $scope.redirectToList = function () {
      $location.path('/productos');
    };
    
    $scope.redirectToDetail = function () {
      $location.path('/productos/' + $scope.product._id);
    };
    
    $scope.goBack = function () {
      if ($scope.editing) {
        $scope.redirectToDetail();
      } else {
        $scope.redirectToList();
      }
    };
    
    $scope.onSave = function () {
      $timeout($scope.goBack, 1000);
    };
    
    $scope.onCancel = function () {
      $scope.goBack();
    };
  });
