angular.module('mimApp')
  .controller('ProductDetailCtrl', function ($scope, $routeParams, $location, $timeout, Product) {
    var productId = $routeParams.productId;
    
    $scope.deleted = false;
    
    $scope.product = Product.get({ id: productId });
    
    $scope.product.$promise.catch(function () {
      $scope.redirectToList();
    });
    
    $scope.redirectToList = function () {
      $location.path('/productos');
    };
    
    $scope.handleDeleteClick = function () {
      if (!confirm('Esta operacion no se puede deshacer. Desea continuar?'))
        return;
      
      $scope.product.$delete(function () {
        $scope.deleted = true;
        $timeout($scope.redirectToList, 1000);
      });
    };
  });
