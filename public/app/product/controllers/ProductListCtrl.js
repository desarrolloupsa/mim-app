angular.module('mimApp')
  .controller('ProductListCtrl', function ($scope, $http, $location) {
    var self = $scope;
    
    self.tableColumns = [
      { key: 'name',        name: 'Nombre' }
    ];
    
    self.handleProductClick = function (product) {
      $location.path('/productos/' + product._id);
    };
  });
