angular.module('mimApp')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/productos', {
        templateUrl: 'app/product/templates/product-list.html',
        controller: 'ProductListCtrl'
      })
      .when('/productos/nuevo', {
        templateUrl: 'app/product/templates/product-editor.html',
        controller: 'ProductEditorCtrl'
      })
      .when('/productos/:productId', {
        templateUrl: 'app/product/templates/product-detail.html',
        controller: 'ProductDetailCtrl'
      })
      .when('/productos/:productId/modificar', {
        templateUrl: 'app/product/templates/product-editor.html',
        controller: 'ProductEditorCtrl'
      });
  });
