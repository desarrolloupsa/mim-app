angular.module('mimApp')
  .directive('autofocus', function($timeout) {
    return {
      restrict: 'A',
      link : function($scope, $element, $attrs) {
        $scope.$watch($attrs.autofocus, function () {
          $timeout(function() {
            $element[0].focus();
          }, 100);
        });
      }
    }
  });