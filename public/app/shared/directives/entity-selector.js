angular.module('mimApp')
  .directive('entitySelector', function ($q, $http, $parse) {
    return {
      require: 'ngModel',
      scope: { },
      restrict: 'E',
      templateUrl: 'app/shared/directives/entity-selector.html',
      link: link
    };
    
    function link($scope, elem, attr, ngModel) {
      var engine = new Bloodhound({
        prefetch: {
          url: attr.prefetchUrl,
          ttl: 0
        },
        datumTokenizer: function (item) {
          return Bloodhound.tokenizers.whitespace(item[attr.displayKey]);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
      });
      
      engine.initialize();
      
      var $input = elem.find('input');
      
      if (attr.innerClass) {
        $input.addClass(attr.innerClass);
      }
      
      var typeaheadDataset = {
        source: engine.ttAdapter(),
        displayKey: attr.displayKey,
        templates: { }
      };
      
      $input.typeahead(null, typeaheadDataset);
      
      $input.on('change', function () {
        ngModel.$setViewValue(null);
      });
      
      $input.on('typeahead:selected typeahead:autocompleted', function (ev, item, dataset) {
        ngModel.$setViewValue(item[attr.outputKey]);
      });
      
      ngModel.$asyncValidators.entityExists = function (modelValue, viewValue) {
        var value = modelValue || viewValue;
        var deferred = $q.defer();
        
        $http.get(attr.prefetchUrl + '/' + value)
          .success(function (item) {
            deferred.resolve();
            $scope.query = item[attr.displayKey];
            $input.typeahead('val', item[attr.displayKey]);
          })
          .error(function () {
            deferred.reject();
          });
        
        return deferred.promise;
      };
    }
  });
