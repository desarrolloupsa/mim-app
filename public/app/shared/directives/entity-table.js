angular.module('mimApp')
  .directive('entityTable', function ($http, $parse) {
    return {
      scope: {
        tableColumns: '=',
        pageCountLimit: '=',
        preprocess: '='
      },
      restrict: 'E',
      templateUrl: 'app/shared/directives/entity-table.html',
      link: link
    };
    
    function link($scope, $elem, $attrs) {
      $scope.loading = false;
      $scope.failed = false;
      
      $scope.entities = [];
      $scope.orderKey = '';
      $scope.orderAsc = true;
      
      $scope.currentPage = 0;
      
      $scope.loadEntities = function () {
        $scope.loading = true;
        
        var opts = {
          params: {
            pagination: {
              skip: $scope.currentPage * $scope.pageCountLimit,
              limit: $scope.pageCountLimit,
            }
          }
        };
        
        if ($scope.orderKey) {
          opts.params.pagination.sort = {};
          opts.params.pagination.sort[$scope.orderKey] = $scope.orderAsc ? 1 : -1;
        }
        
        var req = $http.get('/api/' + $attrs.resourceName, opts)
          .success(function (data, status, $headers) {
            $scope.entities = data;
            
            if ($scope.preprocess) {
              $scope.entities.forEach(function (entity) {
                var promise = $scope.preprocess(entity);
                // TODO: loading = true; after promises
              });
            }
            
            var totalEntities = parseInt($headers('X-Pagination-Total'));
            
            $scope.totalPages = Math.ceil(totalEntities / $scope.pageCountLimit);
          })
          .error(function () {
            $scope.failed = true;
          })
          .finally(function () {
            $scope.loading = false;
          });
      };
      
      $scope.getPageArray = function () {
        return Array($scope.totalPages);
      };
      
      $scope.goToPage = function (i) {
        if (i < 0 || i >= $scope.totalPages) return;
        
        $scope.currentPage = i;
        $scope.loadEntities();
      };
      
      $scope.prevPage = function () {
        $scope.goToPage($scope.currentPage - 1);
      };
      
      $scope.nextPage = function () {
        $scope.goToPage($scope.currentPage + 1);
      };
      
      $scope.setOrd = function (col) {
        if (col.noOrder) return;
        
        var asc = true;;
        
        if ($scope.orderKey === col.key) {
          asc = !$scope.orderAsc;
        }
        
        $scope.orderKey = col.key;
        $scope.orderAsc = asc;
        
        $scope.loadEntities();
      };
      
      $scope.getOrd = function () {
        return ($scope.orderAsc ? '' : '-') + $scope.orderKey;
      };
      
      $scope.entityClick = function (entity) {
        $parse($attrs.onEntityClick)($scope.$parent, { $entity: entity });
      };
      
      $scope.$watch($attrs.resourceName, $scope.loadEntities);
      
      if ($attrs.defaultOrder) {
        var key = $attrs.defaultOrder;
        var asc = true;
        
        if ($attrs.defaultOrder[0] === '-') {
          key = $attrs.defaultOrder.slice(1);
          asc = false;
        }
        
        $scope.orderKey = key;
        $scope.orderAsc = asc;
      }
    }
  });
