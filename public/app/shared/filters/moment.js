angular.module('mimApp')
  .filter('momentFmt', function () {
    return function (input, outFmt, inFmt) {
      if (!input) return '';
      var md = moment(input, inFmt);
      if (!md.isValid()) return '';
      return md.format(outFmt);
    };
  });
