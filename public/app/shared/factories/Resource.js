function appendTransform(defaults, transform) {
  defaults = angular.isArray(defaults) ? defaults : [defaults];
  return defaults.concat(transform);
}

angular.module('mimApp')
  .factory('Resource', function ($resource, $http) {
    return function (url, params, actions, responseTransforms) {
      var defaultActions = {
        'get':    { method: 'GET',    isArray: false },
        'save':   { method: 'POST',   isArray: false },
        'query':  { method: 'GET',    isArray: true  },
        'remove': { method: 'DELETE', isArray: false },
        'delete': { method: 'DELETE', isArray: false },
        'update': { method: 'PUT',    isArray: false },
        'create': { method: 'POST',   isArray: false }
      };
      
      actions = angular.extend(defaultActions, actions);
      
      Object.keys(actions).forEach(function (actionName) {
        actions[actionName].transformResponse =
          appendTransform($http.defaults.transformResponse, responseTransforms);
      });
      
      var defaultParams = {
        id: '@_id'
      };
      
      params = angular.extend(defaultParams, params);
      
      var resource = $resource(url, params, actions);
      
      resource.prototype.$save = function () {
        if (!this._id) {
          return this.$create();
        }
        else {
          return this.$update();
        }
      };
      
      return resource;
    };
  });