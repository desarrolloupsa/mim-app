// === MongoDB

var mongodb_uri = process.env.MONGODB_URI || 'mongodb://localhost:27017/minimim';

var mongoose = require('mongoose');
mongoose.connect(mongodb_uri);

var ObjectId = mongoose.Schema.Types.ObjectId;

var employeeSchema = mongoose.Schema({
  name:           String,
  phoneNumber:    String,
  address:        String,
  birthday:       Date
});

var clientSchema = mongoose.Schema({
  name:           String,
  phoneNumber:    String,
  address:        String,
  birthday:       Date
});

var productSchema = mongoose.Schema({
  name:           String
});

var receiptLineSchema = mongoose.Schema({
  receiptId:            { type: ObjectId, ref: 'Receipt',   index: true },
  employeeId:           { type: ObjectId, ref: 'Employee',  index: true },
  productId:            { type: ObjectId, ref: 'Product',   index: true },
  price:                Number,
  commissionRate:       { type: Number,   default: 0 },
  commissionDiscount:   { type: Number,   default: 0 },
  remunerated:          { type: Boolean,  default: false },
  createdAt:            Date
});

var receiptSchema = mongoose.Schema({
  code:           Number,
  clientId:       { type: ObjectId, ref: 'Client',    index: true },
  createdAt:      Date
});

var Employee        = mongoose.model('Employee',      employeeSchema);
var Client          = mongoose.model('Client',        clientSchema);
var Product         = mongoose.model('Product',       productSchema);
var Receipt         = mongoose.model('Receipt',       receiptSchema);
var ReceiptLine     = mongoose.model('ReceiptLine',   receiptLineSchema);

var resourceMap = {
  'employees':      Employee,
  'clients':        Client,
  'products':       Product,
  'receipts':       Receipt,
  'receipt-lines':  ReceiptLine
};

var ObjectId = mongoose.Types.ObjectId;

// === extend

var Promise     = require('mongoose/lib/promise');

function extendPromise(k, v) {
  Promise.prototype[k] = Promise.prototype.__proto__[k] = v;
}

extendPromise('withThis', function (f) {
  return f(this);
});

extendPromise('ifThen', function (p, q) {
  return this.then(function (data) {
    if (p.apply(this, arguments)) {
      return q(data);
    } else {
      return data;
    }
  });
});

function isNull(o) {
  return o === null;
}

function isFalsy(x) {
  return !x;
}

function doThrow(T) {
  return function () {
    throw new T();
  };
}

function getProp(p) {
  return function (o) {
    return o[p];
  };
}

// === express

var express       = require('express');
var bodyParser    = require('body-parser');
var _             = require('underscore');
var Q             = require('q');

function EntityNotFound() { }

EntityNotFound.prototype.handleResponse = function (res) {
  res.status(404).send({
    error: 'entity not found'
  });
};

function respond(res) {
  return function (promise) {
    promise.then(function (obj) {
      res.send(obj);
    }, function (err) {
      console.log(err);
      
      if (err.handleResponse) {
        return err.handleResponse(res);
      }
      
      res.status(500).send({
        error: err.message
      });
    })
    .end();
  };
}

var app = express();

app.use(express.static(__dirname + '/public'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));

app.use(bodyParser.json());

Object.keys(resourceMap).forEach(function (resourceName) {
  var ResourceModel = resourceMap[resourceName];
  
  app.route('/api/' + resourceName)
    .get(function (req, res) {
      var query = JSON.parse(req.query.query || "{}");
      var pagination = JSON.parse(req.query.pagination || "{}");
      
      var dataPromise =
        ResourceModel
          .find(query, null, pagination)
          .exec();
      
      if (req.query.pagination) {
        var countPromise =
          ResourceModel
            .count(query)
            .exec();
        
        Q.all([dataPromise, countPromise])
          .then(function (results) {
            var data  = results[0];
            var count = results[1];
            
            res.set('X-Pagination-Total', String(count));
            
            res.send(data);
          });
      } else {
        dataPromise
          .withThis(respond(res));
      }
    })
    .post(function (req, res) {
      ResourceModel
        .create(req.body)
        .withThis(respond(res));
    });
  
  app.route('/api/' + resourceName + '/:id')
    .get(function (req, res) {
      ResourceModel
        .findById(req.params.id)
        .exec()
        .ifThen(isNull, doThrow(EntityNotFound))
        .withThis(respond(res));
    })
    .put(function (req, res) {
      var update = {
        $set: req.body
      };
      
      delete update.$set._id;
      
      ResourceModel
        .findByIdAndUpdate(req.params.id, update)
        .exec()
        .withThis(respond(res));
    })
    .delete(function (req, res) {
      ResourceModel
        .findByIdAndRemove(req.params.id)
        .exec()
        .withThis(respond(res));
    });
  
});

app.get('/api/receipts/:receiptId/lines', function (req, res) {
  var query = JSON.parse(req.query.query || "{}");
  
  ReceiptLine
    .find({ receiptId: req.params.receiptId })
    .find(query)
    .exec()
    .withThis(respond(res));
});

app.get('/api/receipts/:receiptId/lines/total', function (req, res) {
  ReceiptLine
    .aggregate([
      { $match: { receiptId: new ObjectId(req.params.receiptId) } },
      { $group: { _id: '$receiptId', value: { $sum: '$price' } } }
    ])
    .exec()
    .then(getProp(0))
    .ifThen(isFalsy, doThrow(EntityNotFound))
    .withThis(respond(res));
});

app.get('/api/employees/:employeeId/receipt-lines/by-day', function (req, res) {
  var timezone = Number(req.query.timezone) || 0;
  var query = { employeeId: new ObjectId(req.params.employeeId) };
  
  if (req.query.from && req.query.to) {
    query.createdAt = {
      $gt: new Date(req.query.from),
      $lt: new Date(req.query.to)
    };
  }
  
  ReceiptLine
    .aggregate([
      { $match: query },
      {
        $group: {
          _id: {
            $let: {
              vars: {
                'localTime': { $add: ['$createdAt', timezone * 60 * 60 * 1000] }
              },
              in: {
                $dayOfYear: '$$localTime'
              }
            }
          },
          lines: {
            $push: {
              _id: '$_id',
              receiptId: '$receiptId',
              productId: '$productId',
              price: '$price',
              commissionRate: '$commissionRate',
              commissionDiscount: '$commissionDiscount',
              remunerated: '$remunerated',
              createdAt: '$createdAt'
            }
          }
        }
      }
    ])
    .exec()
    .withThis(respond(res));
});

var listenPort = process.env.PORT || 8080;

app.listen(listenPort, function () {
  console.log("Listening on http://localhost:%d", listenPort);
});
